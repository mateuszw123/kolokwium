package com.demo.springboot.rest;

public class Alert {
    String alert;

    public Alert() {
    }

    public Alert(String alert) {
        this.alert = alert;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }
}