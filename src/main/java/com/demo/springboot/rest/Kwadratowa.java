package com.demo.springboot.rest;

public class Kwadratowa extends Kwadrat {

    @Override
    public Results calculate(double a, double b, double c) {
        double delta = b*b-4*a*c;
        double sqrtDelta = Math.sqrt(delta);

        if(delta < 0) {
            return new Results();
        } else if (delta == 0) {
            double x0 = (-b+sqrtDelta)/(2*a);
            return new Results(String.valueOf(x0));
        } else {
            double x1 = (-b-sqrtDelta)/(2*a);
            double x2 = (-b+sqrtDelta)/(2*a);
            return new Results(String.valueOf(x1), String.valueOf(x2));
        }
    }
}
