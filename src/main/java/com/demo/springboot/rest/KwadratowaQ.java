package com.demo.springboot.rest;


import com.demo.springboot.rest.Kwadratowa;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

@RestController
public class KwadratowaQ {

    private final Kwadrat kwadrat;

    public KwadratowaQ(Kwadrat kwadrat) {
        this.kwadrat = kwadrat;
    }

    @GetMapping("/api/math/quadratic-function")
    public ResponseEntity<Results> calculateQuadratic(@RequestParam double a, @RequestParam double b, @RequestParam double c) {
        Results result = kwadrat.calculate(a, b, c);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Alert handleException() {
        return new Alert("Parametry nie zostały wprowadzone");
    }

}